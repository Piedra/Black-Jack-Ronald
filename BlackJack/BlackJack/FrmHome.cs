﻿using BlackJackBOL;
using BlackJackENL;
using Facebook;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlackJack
{
    public partial class FrmHome : Form
    {
        private string openToken;
        private UsserBOL ubol;
        private EUsser nativeUsser;
        private EPartida partida;
        private PartidaBOL jbol;

        public FrmHome()
        {
            InitializeComponent();
            openToken = "";
            ubol = new UsserBOL();

        }

        public FrmHome(EUsser usuario, string opentoken)
        {
            InitializeComponent();
            CenterToScreen();
            ubol = new UsserBOL();
            jbol = new PartidaBOL();
            nativeUsser = new EUsser();
            openToken = opentoken;
            ValidateUsser(usuario);
        }

        /// <summary>
        /// Valida un Usuario para saber si ya exite en la base de datos 
        /// si no lo hace nuevo
        /// </summary>
        /// <param name="uss">Usuario a Validar</param>
        private void ValidateUsser(EUsser uss)
        {
            nativeUsser = ubol.Validate(uss);
            Refre();
        }
        /// <summary>
        /// Carga los datos del Usuario
        /// </summary>
        /// <param name="uss"></param>
        private void CargarDatos(EUsser uss)
        {
            pbProfile.Image = uss.PhotoOfUsser;
            lblName.Text = uss.Name;
            lblEmail.Text = uss.Email;
            lblMoney.Text = uss.Money.ToString();

        }

        private void FrmHome_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var fb = new FacebookClient();
                var logoutUrl = fb.GetLogoutUrl(new
                {
                    next = "https://www.facebook.com/connect/login_success.html",
                    access_token = openToken
                });
                var webBrowser = new WebBrowser();
                webBrowser.Navigated += (o, args) =>
                {
                    if (args.Url.AbsoluteUri == "https://www.facebook.com/connect/login_success.html")
                        Close();
                };
                webBrowser.Navigate(logoutUrl.AbsoluteUri);
            }
            catch (Exception)
            {
                Console.WriteLine("Ups, It Seems like You didn't LogOut Right");
            }
        }

        private void FrmHome_FormClosing(object sender, FormClosingEventArgs e)
        {
            FrmLogin frm = new FrmLogin();
            frm.Show();
            Hide();
        }
        /// <summary>
        /// Refresca los datos
        /// </summary>
        private void Refre()
        {
            CargarDatos(nativeUsser);
        }

        private void btnBanco_Click(object sender, EventArgs e)
        {
            EUsser aux = nativeUsser;
            nativeUsser = ubol.Recargar(aux,500);
            Refre();
        }

        private void btnGeneral_Click(object sender, EventArgs e)
        {
            partida = jbol.BuscarPartidaPub(nativeUsser);
            if (partida?.Id > 0)
            {
                FrmMesa frm = new FrmMesa(partida, nativeUsser,openToken);
                frm.Show(this);
                Hide();
            }
            
        }

        private void btnPrivada_Click(object sender, EventArgs e)
        {
            FrmPrivada frm = new FrmPrivada(nativeUsser,openToken);
            frm.Show();
            Hide();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            FrmGrafica frm = new FrmGrafica(nativeUsser);
            frm.Show(this);
            Hide();
        }
    }
}
