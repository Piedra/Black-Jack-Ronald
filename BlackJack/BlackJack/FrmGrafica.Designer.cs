﻿namespace BlackJack
{
    partial class FrmGrafica
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.chartGanancias = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.dgvReportes = new System.Windows.Forms.DataGridView();
            this.dtpReportes = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.lblError = new System.Windows.Forms.Label();
            this.eReporteBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idjugDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.inicialDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.finalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fechaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.chartGanancias)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReportes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eReporteBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // chartGanancias
            // 
            chartArea3.Name = "ChartArea1";
            this.chartGanancias.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.chartGanancias.Legends.Add(legend3);
            this.chartGanancias.Location = new System.Drawing.Point(401, 12);
            this.chartGanancias.Name = "chartGanancias";
            series5.ChartArea = "ChartArea1";
            series5.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            series5.Legend = "Legend1";
            series5.Name = "Inicial";
            series6.ChartArea = "ChartArea1";
            series6.Color = System.Drawing.Color.Red;
            series6.Legend = "Legend1";
            series6.Name = "Final";
            this.chartGanancias.Series.Add(series5);
            this.chartGanancias.Series.Add(series6);
            this.chartGanancias.Size = new System.Drawing.Size(397, 432);
            this.chartGanancias.TabIndex = 0;
            this.chartGanancias.Text = "chart1";
            // 
            // dgvReportes
            // 
            this.dgvReportes.AllowUserToAddRows = false;
            this.dgvReportes.AutoGenerateColumns = false;
            this.dgvReportes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvReportes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.idjugDataGridViewTextBoxColumn,
            this.inicialDataGridViewTextBoxColumn,
            this.finalDataGridViewTextBoxColumn,
            this.fechaDataGridViewTextBoxColumn});
            this.dgvReportes.DataSource = this.eReporteBindingSource;
            this.dgvReportes.Location = new System.Drawing.Point(12, 139);
            this.dgvReportes.Name = "dgvReportes";
            this.dgvReportes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvReportes.Size = new System.Drawing.Size(383, 251);
            this.dgvReportes.TabIndex = 2;
            this.dgvReportes.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvReportes_CellMouseClick);
            // 
            // dtpReportes
            // 
            this.dtpReportes.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpReportes.Location = new System.Drawing.Point(16, 49);
            this.dtpReportes.Name = "dtpReportes";
            this.dtpReportes.Size = new System.Drawing.Size(333, 27);
            this.dtpReportes.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(23, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(283, 21);
            this.label1.TabIndex = 4;
            this.label1.Text = "Seleccióne la Fecha de sus Partidas\r\n";
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblError.Location = new System.Drawing.Point(12, 402);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(22, 21);
            this.lblError.TabIndex = 5;
            this.lblError.Text = "...";
            // 
            // eReporteBindingSource
            // 
            this.eReporteBindingSource.DataSource = typeof(BlackJackENL.EReporte);
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.Visible = false;
            // 
            // idjugDataGridViewTextBoxColumn
            // 
            this.idjugDataGridViewTextBoxColumn.DataPropertyName = "Id_jug";
            this.idjugDataGridViewTextBoxColumn.HeaderText = "Id_jug";
            this.idjugDataGridViewTextBoxColumn.Name = "idjugDataGridViewTextBoxColumn";
            this.idjugDataGridViewTextBoxColumn.Visible = false;
            // 
            // inicialDataGridViewTextBoxColumn
            // 
            this.inicialDataGridViewTextBoxColumn.DataPropertyName = "Inicial";
            this.inicialDataGridViewTextBoxColumn.HeaderText = "Inicial";
            this.inicialDataGridViewTextBoxColumn.Name = "inicialDataGridViewTextBoxColumn";
            this.inicialDataGridViewTextBoxColumn.Width = 120;
            // 
            // finalDataGridViewTextBoxColumn
            // 
            this.finalDataGridViewTextBoxColumn.DataPropertyName = "Final";
            this.finalDataGridViewTextBoxColumn.HeaderText = "Final";
            this.finalDataGridViewTextBoxColumn.Name = "finalDataGridViewTextBoxColumn";
            this.finalDataGridViewTextBoxColumn.Width = 120;
            // 
            // fechaDataGridViewTextBoxColumn
            // 
            this.fechaDataGridViewTextBoxColumn.DataPropertyName = "Fecha";
            this.fechaDataGridViewTextBoxColumn.HeaderText = "Fecha";
            this.fechaDataGridViewTextBoxColumn.Name = "fechaDataGridViewTextBoxColumn";
            this.fechaDataGridViewTextBoxColumn.Width = 120;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(16, 82);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(133, 51);
            this.button1.TabIndex = 6;
            this.button1.Text = "GO";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FrmGrafica
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Coral;
            this.ClientSize = new System.Drawing.Size(810, 456);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtpReportes);
            this.Controls.Add(this.dgvReportes);
            this.Controls.Add(this.chartGanancias);
            this.Name = "FrmGrafica";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmGrafica";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmGrafica_FormClosing);
            this.Load += new System.EventHandler(this.FrmGrafica_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chartGanancias)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReportes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eReporteBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chartGanancias;
        private System.Windows.Forms.DataGridView dgvReportes;
        private System.Windows.Forms.DateTimePicker dtpReportes;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idjugDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn inicialDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn finalDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fechaDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource eReporteBindingSource;
        private System.Windows.Forms.Button button1;
    }
}