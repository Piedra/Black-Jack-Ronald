﻿using BlackJackBOL;
using BlackJackENL;
using Facebook;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlackJack
{
    public partial class FrmBackLogin : Form
    {
        private EFacebook facebook;
        private FacebookBOL fbol;
        private EUsser usser;
        private string openToken;

        public FrmBackLogin()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Indica con que plataforma se esta iniciando Secion
        /// </summary>
        /// <param name="opcion">Numero de la Opcion</param>
        public FrmBackLogin(int opcion)
        {
            InitializeComponent();
            CenterToScreen();
            facebook = new EFacebook
            {
                AppId = "179123749398495",
                Secret = "b2db339e65380a4cb76dd033a93e6f0a",
                Token = "",
                RedUrl = "https://www.facebook.com/connect/login_success.html"
            };
            fbol = new FacebookBOL();
            usser = new EUsser();
            openToken = "";
            LoginSeleccion(opcion);
        }
        
        private void FrmBackLogin_Load(object sender, EventArgs e)
        {
            fbol = new FacebookBOL();
            openToken = "";
        }
        /// <summary>
        /// Toma Accion al saber que Con que plataforma se Inicio
        /// </summary>
        /// <param name="opcion">int la opcion</param>
        private void LoginSeleccion(int opcion)
        {
            switch (opcion)
            {
                case 1:
                    DoFacebookLogin();
                    break;
                
            }
        }
        /// <summary>
        /// Realiza todos los pasos para ingresar con una cuenta facebook
        /// </summary>
        private void DoFacebookLogin()
        {
            try
            {
                var loginUrl = fbol.GetLoginUrl(facebook);
                wbBrowser.Navigate(loginUrl);
            }
            catch (Exception ex)
            {

                throw new Exception("Facebokk API isn't Responding. Please Try Agin Later"); 
            }
        }

        private void wbBrowser_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            var fb = new FacebookClient();
            FacebookOAuthResult oauthResult;
            if (!fb.TryParseOAuthCallbackUrl(e.Url, out oauthResult))
            {
                return;
            }
            if (oauthResult.IsSuccess)
            {
                LoginSucceeded(oauthResult);
            }
            else {
                Console.WriteLine("Error");
                }
        }
        /// <summary>
        /// Indica si se ha ingresado con Exito
        /// </summary>
        /// <param name="oauthResult">El indicador</param>
        public void LoginSucceeded(FacebookOAuthResult oauthResult)
        {
            // Hide the Web browser
            wbBrowser.IsWebBrowserContextMenuEnabled = false;
            // Grab the access token (necessary for further operations)
            var logToken = GetAccessToken(oauthResult);
            openToken = logToken;
            var aux = new FacebookClient(openToken);
            dynamic data = aux.Get("/me/?fields=email,name");//Investigar mas
            WebClient wc = new WebClient();
            byte[] bytes = wc.DownloadData("http://graph.facebook.com/" + data.id+ "/picture?type=large");
            MemoryStream ms = new MemoryStream(bytes);
            Image foto = Image.FromStream(ms);
            usser.Name = data.name;
            usser.Email = data.email;
            usser.UsserId = data.id;
            usser.PhotoOfUsser = foto;
            FrmHome frm = new FrmHome(usser,openToken);
            frm.Show();
            Hide();
        }
        /// <summary>
        /// Si consigue el token Del Usuario
        /// </summary>
        /// <param name="oauthResult"></param>
        /// <returns></returns>
        public string GetAccessToken(FacebookOAuthResult oauthResult)
        {
            var client = new FacebookClient();
            dynamic result = client.Get("oauth/access_token",
            new
            {
                client_id = facebook.AppId,
                client_secret = facebook.Secret,
                redirect_uri = facebook.RedUrl,
                code = oauthResult.Code
            });
            return result.access_token;
        }
        

        private void FrmBackLogin_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Owner != null)
            {
                Owner.Show();
                Owner.Refresh();
            }
        }
    }
}
