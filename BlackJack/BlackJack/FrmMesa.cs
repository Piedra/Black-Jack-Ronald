﻿using BlackJackBOL;
using BlackJackENL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Speech.Recognition;

namespace BlackJack
{
    public partial class FrmMesa : Form
    {

        private EPartida partida;
        private EUsser usu;
        private PartidaBOL jbol;
        private UsserBOL ubol;
        private int apuesta;
        private EDeck deck;
        private string deckId;
        private int sumUsu;
        private int sumDiler;
        private string optoken;
        private bool microphone;
        private SpeechRecognitionEngine escuchar;
        private EReporte miReporte;
        

        public FrmMesa()
        {
            InitializeComponent();
        }
        public FrmMesa(EPartida party, EUsser uss, string token)//Public Party//StarWith Diler
        {
            InitializeComponent();
            jbol = new PartidaBOL();
            ubol = new UsserBOL();
            miReporte = new EReporte();
            deck = new EDeck();
            microphone = false;
            usu = uss;
            miReporte.Inicial = Convert.ToInt32(usu.Money);
            partida = party;
            escuchar = new SpeechRecognitionEngine();
            jbol.SetDilerToTable(party);
            optoken = token;
            sumUsu = 0;
            sumDiler = 0;
            StartGame();
            deckId = jbol.GetMeDeck(party);
            ShuffleCards();
            apuesta = 0;
            Refrescar();
            timer1.Start();
            timer2.Start();
            DilerGame.Start();
            SitOtherPlayers();
            MyData();

        }

        private void FrmMesa_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Se encargar de Sentar A Todos Los Jugadores
        /// </summary>
        private void SitOtherPlayers()
        {
            //Cargar Los jugadores de la mesa
            //Por Partida Id
            //Arreglar
            List<EUsser> usserInGame = ubol.CargarUsuariosEnPartida(partida);

            foreach (EUsser item in usserInGame)
            {
                foreach (Control obj in Controls)
                {
                    if (obj is Label)
                    {
                        if (obj.Name.Equals("lblJug" + item.Id))
                        {
                            obj.Text = item.Name;

                        }
                    }
                }
            }
        }
        /// <summary>
        /// Refresca Los datos de la Partida
        /// </summary>
        private void Refrescar()
        {
            partida = jbol.CargarPartida(partida);
            lblEstado.Text = "Jugando .... " + partida.JugadorActual;//CargarUsuarioID( partida.JugadorActual)
            btnPlantarse.Enabled = partida.JugadorActual == usu.Id;
            SitOtherPlayers();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Refrescar();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            MyData();
        }
        
        private void btn500_Click(object sender, EventArgs e)
        {
            try
            {
                if (usu.Money >= apuesta + 500)
                {
                    apuesta += 500;
                    MarcarApuesta();
                }
                else
                {
                    lblError.Text = "No tiene la Cantidad de Dinero Suficiete.";
                }
            }
            catch (Exception ex)
            {
                lblError.Text = "Intente Nuevamente" + ex.Message;
            }

        }

        private void btn100_Click(object sender, EventArgs e)
        {
            try
            {
                if (usu.Money >= apuesta + 100)
                {
                    apuesta += 100;
                    MarcarApuesta();
                }
                else
                {
                    lblError.Text = "No tiene la Cantidad de Dinero Suficiete.";
                }

            }
            catch (Exception ex)
            {
                lblError.Text = "Intente Nuevamente" + ex.Message;
            }
        }

        private void btn50_Click(object sender, EventArgs e)
        {
            try
            {
                if (usu.Money >= apuesta + 50)
                {
                    apuesta += 50;
                    MarcarApuesta();
                }
                else
                {
                    lblError.Text = "No tiene la Cantidad de Dinero Suficiete.";
                }

            }
            catch (Exception ex)
            {
                lblError.Text = "Intente Nuevamente" + ex.Message;
            }

        }
        /// <summary>
        /// Marca la Apuesta en un Label
        /// </summary>
        private void MarcarApuesta()
        {
            lblTotal.Text = apuesta.ToString();
        }

        private void btnReiApuesta_Click(object sender, EventArgs e)
        {
            try
            {
                apuesta = 0;
                MarcarApuesta();
            }
            catch (Exception ex)
            {

                lblError.Text = "No se pudo Realizar la Apuesta";
            }
        }

        private void btnApostar_Click(object sender, EventArgs e)
        {
            try
            {
                if (jbol.RestarDinero(usu, apuesta))
                {
                    picApuesta.Image = Image.FromFile(@"C:\Users\Fabricio\Downloads\BalckJack\ChipBet.png");
                    lblTotal.Text = "0";
                }
                else
                {
                    lblError.Text = "No se pudo completar La Transaccion";
                }
            }
            catch (Exception ex)
            {
                lblError.Text = "Intente Nuevmente" + ex.Message;
            }
        }
        /// <summary>
        /// Realiza una serie de Acciones para Comenzar el Juego
        /// </summary>
        private void StartGame()
        {
            picb_play_1.Visible = false;
            picb_play_1.Enabled = false;
            picb_play_4.Visible = false;
            picb_play_4.Enabled = false;
            picb_play_5.Visible = false;
            picb_play_5.Enabled = false;
            picb_play_2.Image = null;
            picb_play_3.Image = null;
            picApuesta.Image = null;
        }
        /// <summary>
        /// Se Encargasr de refrescar los datos del Usuario 
        /// </summary>
        private void MyData()
        {
            //Mas Datos de Ser necesario
            usu = ubol.Refresh(usu.Id);
            lblMiDinero.Text = usu.Money.ToString();
        }
        
        private void btnCarta_Click(object sender, EventArgs e)
        {
            try
            {
                PedirCartas();
            }
            catch (Exception ex)
            {

                lblError.Text = "Intente Nuevamente";
            }
        }
        /// <summary>
        /// Pide Carta
        /// </summary>
        private void PedirCartas()
        {
            if (picb_play_1.Visible && !(picb_play_4.Visible))
            {
                picb_play_4.Visible = true;
                picb_play_4.Enabled = true;
                ECarta cartas = NewCard(deckId, "1");
                WebClient wc = new WebClient();
                byte[] bytes = wc.DownloadData(cartas.cards.ElementAt(0).image);
                MemoryStream ms = new MemoryStream(bytes);
                Image aux = Image.FromStream(ms);
                picb_play_4.Image = aux;
                sumUsu += ChargeNumbers(cartas.cards.ElementAt(0).value.ToString());
                if (sumUsu > 21)
                {
                    jbol.Plantarse(partida);
                }
            }
            else if (picb_play_4.Visible && picb_play_1.Visible)
            {
                picb_play_5.Visible = true;
                picb_play_5.Enabled = true;
                ECarta cartas = NewCard(deckId, "1");
                WebClient wc = new WebClient();
                byte[] bytes = wc.DownloadData(cartas.cards.ElementAt(0).image);
                MemoryStream ms = new MemoryStream(bytes);
                Image aux = Image.FromStream(ms);
                picb_play_5.Image = aux;
                sumUsu += ChargeNumbers(cartas.cards.ElementAt(0).value.ToString());
                if (sumUsu > 21)
                {
                    jbol.Plantarse(partida);
                }
            }
            else
            {
                picb_play_1.Visible = true;
                picb_play_1.Enabled = true;
                ECarta cartas = NewCard(deckId, "1");
                WebClient wc = new WebClient();
                byte[] bytes = wc.DownloadData(cartas.cards.ElementAt(0).image);
                MemoryStream ms = new MemoryStream(bytes);
                Image aux = Image.FromStream(ms);
                picb_play_1.Image = aux;
                sumUsu += ChargeNumbers(cartas.cards.ElementAt(0).value.ToString());
                if (sumUsu > 21)
                {
                    jbol.Plantarse(partida);
                }
            }
        }
        /// <summary>
        /// Hace un Nuevo Deck Apartir del API
        /// </summary>
        /// <returns></returns>
        private EDeck NewDeck()
        {
            WebRequest request = WebRequest.Create("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=4");
            request.Credentials = CredentialCache.DefaultCredentials;
            WebResponse response = request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();
            reader.Close(); response.Close();
            EDeck deck = Newtonsoft.Json.JsonConvert.DeserializeObject<EDeck>(responseFromServer);
            return deck;
        }
        /// <summary>
        /// Saca Cartas del deck.
        /// </summary>
        /// <param name="idDeck">ID DEL DECK</param>
        /// <param name="cantCards">Cantidad de cartas</param>
        /// <returns>Una Carta</returns>
        private ECarta NewCard(string idDeck, string cantCards)
        {
            WebRequest request = WebRequest.Create("https://deckofcardsapi.com/api/deck/" + idDeck + "/draw/?count=" + cantCards);
            request.Credentials = CredentialCache.DefaultCredentials;
            WebResponse response = request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();
            reader.Close(); response.Close();
            ECarta carta = Newtonsoft.Json.JsonConvert.DeserializeObject<ECarta>(responseFromServer);
            return carta;
        }
        /// <summary>
        /// hace un Nuevo deck
        /// </summary>
        /// <returns></returns>
        private EDeck AskForDeck()
        {
            return new EDeck();
        }
        /// <summary>
        /// Se Encarga de repartir las Cartas
        /// </summary>
        private void ShuffleCards()
        {
            ECarta cartas = NewCard(deckId, "2");
            WebClient wc = new WebClient();
            byte[] byts = wc.DownloadData(cartas.cards.ElementAt(0).image);
            byte[] bytes = wc.DownloadData(cartas.cards.ElementAt(1).image);
            MemoryStream msa = new MemoryStream(byts);
            MemoryStream ms = new MemoryStream(bytes);
            Image aux2 = Image.FromStream(msa);
            Image aux = Image.FromStream(ms);
            picb_play_2.Image = aux;
            picb_play_3.Image = aux2;
            sumUsu = ChargeNumbers(cartas.cards.ElementAt(0).value.ToString());
            sumUsu += ChargeNumbers(cartas.cards.ElementAt(1).value.ToString());
            Console.WriteLine(sumUsu);
        }
        /// <summary>
        /// Toma los Valores String de las cartas y los voncierte en numeros
        /// </summary>
        /// <param name="num">string con el valor</param>
        /// <returns>Num del valor</returns>
        private int ChargeNumbers(string num)
        {
            switch (num)
            {
                case "2":
                    return 2;
                case "3":
                    return 3;
                case "4":
                    return 4;
                case "5":
                    return 5;
                case "6":
                    return 6;
                case "7":
                    return 7;
                case "8":
                    return 8;
                case "9":
                    return 9;
                case "10":
                    return 10;
                case "ACE":
                    return 11;
                case "QUEEN":
                    return 10;
                case "JACK":
                    return 10;
                case "KING":
                    return 10;
            }
            return 1;
        }
        /// <summary>
        /// Inidica si gano el usuario o el Diler
        /// </summary>
        private void DeterminarGanador()
        {
            if ((sumUsu > sumDiler) && (sumUsu <= 21))
            {
                Console.WriteLine("Gano El Usuario");
                int tot = apuesta * 2;
                EUsser aux = usu;
                usu = ubol.Recargar(aux, tot);
                MyData();
            }
            else if (sumDiler > 21)
            {
                Console.WriteLine("Gano User");
                int tot = apuesta * 2;
                EUsser aux = usu;
                usu = ubol.Recargar(aux, tot);
                MyData();
            }
            else
            {
                Console.WriteLine("Gano Diler");
            }
        }

        private void DilerGame_Tick(object sender, EventArgs e)
        {
            if (partida.JugadorActual == 2)
            {
                DilerBehavior();
                DeterminarGanador();
                StartGame();
                ShuffleCards();
                jbol.Plantarse(partida);
                EDeck aux = NewDeck();
                apuesta = 0;
                jbol.SetNewDeck(aux, partida);
            }
        }
        /// <summary>
        /// Controlas Las Acciones del Diler
        /// </summary>
        private void DilerBehavior()
        {
            ECarta cartas = NewCard(deckId, "2");
            sumDiler = ChargeNumbers(cartas.cards.ElementAt(0).value.ToString());
            sumDiler += ChargeNumbers(cartas.cards.ElementAt(1).value.ToString());
            if (sumDiler >= 17)
            {
                jbol.Plantarse(partida);
            }
            else
            {
                while (sumDiler <= 16)
                {
                    cartas = NewCard(deckId, "1");
                    sumDiler += ChargeNumbers(cartas.cards.ElementAt(0).value.ToString());
                }
            }
        }
        /// <summary>
        /// Lo saca de la Partida
        /// </summary>
        private void GetMeOut()
        {
            //Sacarlo de la BD
            if (jbol.TakeOutOfRoom(usu, partida))
            {
                FrmHome frm = new FrmHome(usu, optoken);
                frm.Show();
                MessageBox.Show("Salio de la Sala con Exito");
                Close();
            }
            else
            {
                MessageBox.Show("Lo Lamentamos Salida Sin Exito");
            }

        }

        private void FrmMesa_FormClosing(object sender, FormClosingEventArgs e)
        {
            miReporte.Id_jug = usu.Id;
            miReporte.Final = Convert.ToInt32(usu.Money);
            String sDate = DateTime.Now.ToString();
            DateTime datevalue = (Convert.ToDateTime(sDate.ToString()));
            String dy = datevalue.Day.ToString();
            String mn = datevalue.Month.ToString();
            String yy = datevalue.Year.ToString();
            miReporte.Fecha = datevalue;
            ReporteBOL rbol = new ReporteBOL();
            rbol.DoMyReport(miReporte); 

            GetMeOut();
        }

        private void btnPlantarse_Click(object sender, EventArgs e)
        {
            jbol.Plantarse(partida);
        }

        private void btnMic_Click(object sender, EventArgs e)
        {
            try
            {
                if (microphone)
                {
                    microphone = false;
                    Console.WriteLine("Se Fue FALSE");
                }
                else
                {
                    microphone = true;
                }
                PutImageMicrophone();
                MicrophoneAccions();

            }
            catch (Exception)
            {

                throw;
            }
        }
        /// <summary>
        /// Hace las Acciones del Microfhono para saber si esta activo
        /// </summary>
        private void MicrophoneAccions()
        {
            try
            {
                if (microphone)
            {
                escuchar.SetInputToDefaultAudioDevice();
                escuchar.LoadGrammar(new DictationGrammar());
                escuchar.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(Lector);
                escuchar.RecognizeAsync(RecognizeMode.Multiple);
            }
            }
            catch (InvalidOperationException)
            {

                lblError.Text = "No se Pueden Abrir Multiples Veces El Mic";
            }
        }
        /// <summary>
        /// Se Encarga de escuchar las palabras
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Lector(object sender,SpeechRecognizedEventArgs e)
        {
            foreach (RecognizedWordUnit palabra in e.Result.Words)
            {
                if (palabra.Text == "card")
                {
                    PedirCartas();
                }
                else if (palabra.Text == "got")
                {
                    apuesta += 500;
                    MarcarApuesta();
                }
                else if (palabra.Text == "back")
                {
                    jbol.Plantarse(partida);
                }
                else if (palabra.Text == "green")
                {
                    apuesta = 0;
                    MarcarApuesta();
                }

                Console.WriteLine(palabra.Text);
            }
        }
        /// <summary>
        /// Coloca la imagen en el button del Micro
        /// </summary>
        private void PutImageMicrophone()
        {
            if (microphone)
            {
                btnMic.BackgroundImage = Image.FromFile(@"C:\Users\Fabricio\Downloads\BalckJack\microphone.png");
            }
            else
            {
                btnMic.BackgroundImage = Image.FromFile(@"C:\Users\Fabricio\Downloads\BalckJack\muted.png");

            }
        }
    }
}
