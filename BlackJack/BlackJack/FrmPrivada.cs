﻿using BlackJackBOL;
using BlackJackENL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlackJack
{
    public partial class FrmPrivada : Form
    {
        private EUsser usu;
        private string originToken;
        private PartidaBOL jbol;



        public FrmPrivada()
        {
            InitializeComponent();
        }
        public FrmPrivada(EUsser uss, string token)
        {
            InitializeComponent();
            usu = uss;
            originToken = token;
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void FrmPrivada_Load(object sender, EventArgs e)
        {
            jbol = new PartidaBOL();
        }

        private void btnNuevaPar_Click(object sender, EventArgs e)
        {
            
            try
            {
                EDeck deckis = NewDeck();
                EPartida aux = new EPartida
                {
                    Id_Deck = deckis.deck_id,
                    CantJugadores = Convert.ToInt32(cbCantJug.Text),
                    JugadorActual = 2,
                    Name = txtNomSala.Text.Trim(),
                    Password = txtPassword.Text.Trim()
                };
                jbol.NuevaPartida(aux);

                if (MandarCorreo())
                {
                    EPartida var = jbol.BuscarPartidaPrivadaF(usu, txtPassword.Text);
                    FrmMesa frm = new FrmMesa(var, usu, originToken);
                    frm.Show();
                    Close();
                }
                else
                {
                    lblErrorPN.Text = "No se pudo enviar el Correo";
                }
                
            }
            catch (Exception ex)
            {

                lblErrorPN.Text = "Intente nuevamente" + ex.Message;
            }



        }
        /// <summary>
        /// Se encarga de Mancar un Correo Al Correo Indicado
        /// </summary>
        /// <returns></returns>
        private bool MandarCorreo()
        {

            try
            {
                string[] correos = txtPara.Text.Split(',');
                foreach (string correo in correos)
                {
                    System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                    msg.To.Add(correo);
                    msg.Subject = "Invitación al Juego Black Jack";
                    msg.SubjectEncoding = System.Text.Encoding.UTF8;
                    msg.Body = " El jugador " + usu.Name + " Te Esta Invitado a un Private Match " +
                        "Info: Para poder ingresar a la mesa debes ingresar los Siguientes Datos: " +
                        " - Nombre de La Partida: " + txtNomSala.Text +
                        "- Password de la Partida: " + txtPassword.Text +
                        " Gracias Te Estaremos Esperando";
                    msg.BodyEncoding = System.Text.Encoding.UTF8;
                    msg.IsBodyHtml = true;
                    msg.From = new System.Net.Mail.MailAddress("balckjackgame@gmail.com");
                    System.Net.Mail.SmtpClient cliente = new System.Net.Mail.SmtpClient();
                    cliente.Credentials = new System.Net.NetworkCredential("balckjackgame@gmail.com", "KIKI0932");
                    cliente.Port = 587;
                    cliente.EnableSsl = true;
                    cliente.Host = "smtp.gmail.com";
                    cliente.Send(msg);
                }

                return true;
            }
            catch (Exception)
            {

                lblErrorPN.Text = "No se Logro mandar el correo";
            }
            return false;
        }
        /// <summary>
        /// crea un nuevo Deck
        /// </summary>
        /// <returns></returns>
        private EDeck NewDeck()
        {
            WebRequest request = WebRequest.Create("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=4");
            request.Credentials = CredentialCache.DefaultCredentials;
            WebResponse response = request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();
            reader.Close(); response.Close();
            EDeck deck = Newtonsoft.Json.JsonConvert.DeserializeObject<EDeck>(responseFromServer);
            return deck;
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {
            txtInPar.Text = "";
            txtPass.Text = "";
            txtInPar.Enabled = false;
            txtPass.Enabled = false;
            btnInPar.Enabled = false;
            txtPara.Enabled = true;
            txtNomSala.Enabled = true;
            txtPassword.Enabled = true;
            btnNuevaPar.Enabled = true;
        }

        private void gbInPart_Enter(object sender, EventArgs e)
        {
            
            txtPara.Text = "";
            txtNomSala.Text = "";
            txtPassword.Text = "";
            txtPara.Enabled = false;
            txtNomSala.Enabled = false;
            txtPassword.Enabled = false;
            btnNuevaPar.Enabled = false;
            txtInPar.Enabled = true;
            txtPass.Enabled = true;
            btnInPar.Enabled = true;
        }

        private void btnInPar_Click(object sender, EventArgs e)
        {
            try
            {
                EPartida partidilla = jbol.CargarPrivada(txtInPar.Text.Trim(), txtPass.Text.Trim());
                if (partidilla != null)
                {
                    EPartida var = jbol.BuscarPartidaPri(usu,partidilla.Password);
                    FrmMesa frm = new FrmMesa(var,usu,originToken);
                    frm.Show();
                    Hide();
                }
                else
                {
                    lblErrorIP.Text = "No se Encontro Partida";
                }
            }
            catch (Exception ex)
            {

                lblErrorIP.Text = "Intente Nuevamente"+ex.Message;
            }
        }

    }
}
