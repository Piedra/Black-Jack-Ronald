﻿using BlackJackENL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tweetinvi;
using Tweetinvi.Models;

namespace BlackJack
{
    public partial class FrmLogTwitta : Form
    {
        private IAuthenticationContext authenticationContext;

        public FrmLogTwitta()
        {
            InitializeComponent();
            CenterToScreen();
        }

        private void FrmLogTwitta_Load(object sender, EventArgs e)
        {
            GetTheAccesCode();
        }
        
        public void GetTheAccesCode()
        {
            var appCredentials = new TwitterCredentials("KZkAEj5yNNroImi2SoMin7SR1", "QBFueNsISp6O7LtvL99gJq42z9Jk3J0ywMio2RoSbiNapr7uYw");
            authenticationContext = AuthFlow.InitAuthentication(appCredentials);
            Process.Start(authenticationContext.AuthorizationURL);
        }
        /// <summary>
        /// Valida el Pin del Usuario
        /// </summary>
        /// <param name="pin">Pin Ingresado</param>
        /// <returns>Credenciales</returns>
        public IAuthenticatedUser ValidarPIN(string pin)
        {
            try
            {
                // Con este código PIN ahora es posible recuperar las credenciales de Twitter 
                //Aca no me trae LOs DATOS DEL mAE
                var userCredentials = AuthFlow.CreateCredentialsFromVerifierCode(pin,authenticationContext);
                Auth.SetCredentials(userCredentials);   // Use las credenciales del usuario en su aplicación 
                return User.GetAuthenticatedUser();
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Problema al autenticar.", ex);
            }
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(textBox1.Text))
                {
                    throw new Exception("Favor ingrese su código de autentificación.");
                }

                // Obtener los datos del  usuario
                IAuthenticatedUser authenticated = ValidarPIN(textBox1.Text.Trim());
                // Obtener la foto de usuario
                WebClient webRequest = new WebClient();
                System.IO.Stream stm = webRequest.OpenRead(authenticated.ProfileImageUrlFullSize);
                Bitmap btmp = new Bitmap(stm);
                stm.Flush();
                stm.Close();
                pbIma.Image = btmp;
                EUsser us = new EUsser
                {
                    Name = authenticated.Name,
                    Email = authenticated.Email,
                    PhotoOfUsser = pbIma.Image
                };
                Console.WriteLine(us.Email + "eSTE ES EL CORRE");
                
                FrmHome frma = new FrmHome(us, "123");
                frma.Show();
                Hide();
                textBox1.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "BlackJack");
            }
        }
    }
}
