﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlackJack
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
            CenterToScreen();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            FrmBackLogin frm = new FrmBackLogin(1);
            frm.Show(this);
            Hide();
        }

        private void FrmLogin_FormClosing(object sender, FormClosingEventArgs e)
        {
           Application.Exit();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FrmLogTwitta frm = new FrmLogTwitta(); ;
            frm.Show(this);
            Hide();
        }
    }
}
