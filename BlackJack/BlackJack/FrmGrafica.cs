﻿using BlackJackBOL;
using BlackJackENL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlackJack
{
    public partial class FrmGrafica : Form
    {
        private ReporteBOL rbol;
        private EReporte rep;
        private EUsser homeUser;

        public FrmGrafica()
        {
            InitializeComponent();
        }

        public FrmGrafica(EUsser uss)
        {
            InitializeComponent();
            homeUser = uss;
            rbol = new ReporteBOL();
            rep = new EReporte();
        }

        private void FrmGrafica_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            DateTime aux = dtpReportes.Value;
            List<EReporte> reportes = rbol.TakeByDate(homeUser, aux);
            dgvReportes.DataSource = null;
            dgvReportes.DataSource = reportes;
            if (reportes.Count==0)
            {
                lblError.Text = string.Format("No Existen Datos en Esta Fehca {0}",aux.ToShortDateString());
            }
        }

        private void dgvReportes_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            Console.WriteLine("Entro");
            int row = dgvReportes.SelectedRows.Count > 0 ? dgvReportes.SelectedRows[0].Index : -1;
            if (row >= 0)
            {
                EReporte rep = dgvReportes.CurrentRow.DataBoundItem as EReporte;
                DoGraph(rep);
            }
        }
        /// <summary>
        /// Rellena el Grafico con los datos con un reporte
        /// </summary>
        /// <param name="rep">reporte</param>
        private void DoGraph(EReporte rep)
        {
            chartGanancias.Series["Inicial"].Points.Clear();
            chartGanancias.Series["Final"].Points.Clear();
            chartGanancias.Titles.Clear();
            chartGanancias.Series["Inicial"].Points.AddXY(homeUser.Name,rep.Inicial);
            chartGanancias.Series["Final"].Points.AddXY(homeUser.Name, rep.Final);
        }

        private void FrmGrafica_FormClosing(object sender, FormClosingEventArgs e)
        {
            Owner?.Show();
        }
    }
}
