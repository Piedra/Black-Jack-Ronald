﻿namespace BlackJack
{
    partial class FrmPrivada
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPrivada));
            this.gbNewPart = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbCantJug = new System.Windows.Forms.ComboBox();
            this.btnNuevaPar = new System.Windows.Forms.Button();
            this.lblErrorPN = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPara = new System.Windows.Forms.TextBox();
            this.txtNomSala = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.gbInPart = new System.Windows.Forms.GroupBox();
            this.lblErrorIP = new System.Windows.Forms.Label();
            this.txtInPar = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnInPar = new System.Windows.Forms.Button();
            this.txtPass = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.gbNewPart.SuspendLayout();
            this.gbInPart.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbNewPart
            // 
            this.gbNewPart.Controls.Add(this.button2);
            this.gbNewPart.Controls.Add(this.label3);
            this.gbNewPart.Controls.Add(this.cbCantJug);
            this.gbNewPart.Controls.Add(this.btnNuevaPar);
            this.gbNewPart.Controls.Add(this.lblErrorPN);
            this.gbNewPart.Controls.Add(this.label7);
            this.gbNewPart.Controls.Add(this.label1);
            this.gbNewPart.Controls.Add(this.txtPara);
            this.gbNewPart.Controls.Add(this.txtNomSala);
            this.gbNewPart.Controls.Add(this.label2);
            this.gbNewPart.Controls.Add(this.txtPassword);
            this.gbNewPart.Controls.Add(this.label4);
            this.gbNewPart.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbNewPart.Location = new System.Drawing.Point(12, 12);
            this.gbNewPart.Name = "gbNewPart";
            this.gbNewPart.Size = new System.Drawing.Size(450, 253);
            this.gbNewPart.TabIndex = 7;
            this.gbNewPart.TabStop = false;
            this.gbNewPart.Text = "Datos Partida Nueva";
            this.gbNewPart.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(321, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(123, 17);
            this.label3.TabIndex = 17;
            this.label3.Text = "Cantidad de Jug.";
            // 
            // cbCantJug
            // 
            this.cbCantJug.FormattingEnabled = true;
            this.cbCantJug.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7"});
            this.cbCantJug.Location = new System.Drawing.Point(382, 39);
            this.cbCantJug.Name = "cbCantJug";
            this.cbCantJug.Size = new System.Drawing.Size(62, 25);
            this.cbCantJug.TabIndex = 8;
            // 
            // btnNuevaPar
            // 
            this.btnNuevaPar.BackColor = System.Drawing.Color.Transparent;
            this.btnNuevaPar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNuevaPar.BackgroundImage")));
            this.btnNuevaPar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnNuevaPar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNuevaPar.ForeColor = System.Drawing.Color.White;
            this.btnNuevaPar.Location = new System.Drawing.Point(364, 147);
            this.btnNuevaPar.Name = "btnNuevaPar";
            this.btnNuevaPar.Size = new System.Drawing.Size(62, 60);
            this.btnNuevaPar.TabIndex = 16;
            this.btnNuevaPar.UseVisualStyleBackColor = false;
            this.btnNuevaPar.Click += new System.EventHandler(this.btnNuevaPar_Click);
            // 
            // lblErrorPN
            // 
            this.lblErrorPN.AutoSize = true;
            this.lblErrorPN.Location = new System.Drawing.Point(7, 224);
            this.lblErrorPN.Name = "lblErrorPN";
            this.lblErrorPN.Size = new System.Drawing.Size(20, 17);
            this.lblErrorPN.TabIndex = 14;
            this.lblErrorPN.Text = "...";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 120);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 21);
            this.label7.TabIndex = 13;
            this.label7.Text = "Inv. Amigos";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 161);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 17);
            this.label1.TabIndex = 9;
            this.label1.Text = "Para :";
            // 
            // txtPara
            // 
            this.txtPara.Location = new System.Drawing.Point(10, 181);
            this.txtPara.Multiline = true;
            this.txtPara.Name = "txtPara";
            this.txtPara.Size = new System.Drawing.Size(312, 26);
            this.txtPara.TabIndex = 10;
            // 
            // txtNomSala
            // 
            this.txtNomSala.Location = new System.Drawing.Point(10, 39);
            this.txtNomSala.Multiline = true;
            this.txtNomSala.Name = "txtNomSala";
            this.txtNomSala.Size = new System.Drawing.Size(312, 26);
            this.txtNomSala.TabIndex = 8;
            this.txtNomSala.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Contraseña :";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(10, 91);
            this.txtPassword.Multiline = true;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(312, 26);
            this.txtPassword.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(7, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(141, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "Nombre De La Sala :";
            // 
            // gbInPart
            // 
            this.gbInPart.Controls.Add(this.button1);
            this.gbInPart.Controls.Add(this.lblErrorIP);
            this.gbInPart.Controls.Add(this.txtInPar);
            this.gbInPart.Controls.Add(this.label5);
            this.gbInPart.Controls.Add(this.btnInPar);
            this.gbInPart.Controls.Add(this.txtPass);
            this.gbInPart.Controls.Add(this.label6);
            this.gbInPart.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbInPart.Location = new System.Drawing.Point(12, 271);
            this.gbInPart.Name = "gbInPart";
            this.gbInPart.Size = new System.Drawing.Size(450, 161);
            this.gbInPart.TabIndex = 7;
            this.gbInPart.TabStop = false;
            this.gbInPart.Text = "Ingresar A Partida";
            this.gbInPart.Enter += new System.EventHandler(this.gbInPart_Enter);
            // 
            // lblErrorIP
            // 
            this.lblErrorIP.AutoSize = true;
            this.lblErrorIP.Location = new System.Drawing.Point(6, 131);
            this.lblErrorIP.Name = "lblErrorIP";
            this.lblErrorIP.Size = new System.Drawing.Size(20, 17);
            this.lblErrorIP.TabIndex = 15;
            this.lblErrorIP.Tag = "";
            this.lblErrorIP.Text = "...";
            // 
            // txtInPar
            // 
            this.txtInPar.Location = new System.Drawing.Point(6, 37);
            this.txtInPar.Multiline = true;
            this.txtInPar.Name = "txtInPar";
            this.txtInPar.Size = new System.Drawing.Size(312, 26);
            this.txtInPar.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 67);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "Password";
            // 
            // btnInPar
            // 
            this.btnInPar.BackColor = System.Drawing.Color.Transparent;
            this.btnInPar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnInPar.BackgroundImage")));
            this.btnInPar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnInPar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInPar.ForeColor = System.Drawing.Color.White;
            this.btnInPar.Location = new System.Drawing.Point(364, 53);
            this.btnInPar.Name = "btnInPar";
            this.btnInPar.Size = new System.Drawing.Size(62, 60);
            this.btnInPar.TabIndex = 5;
            this.btnInPar.UseVisualStyleBackColor = false;
            this.btnInPar.Click += new System.EventHandler(this.btnInPar_Click);
            // 
            // txtPass
            // 
            this.txtPass.Location = new System.Drawing.Point(6, 87);
            this.txtPass.Multiline = true;
            this.txtPass.Name = "txtPass";
            this.txtPass.Size = new System.Drawing.Size(312, 26);
            this.txtPass.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 17);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(129, 17);
            this.label6.TabIndex = 3;
            this.label6.Text = "Nombre de la Sala";
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(351, 22);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 16;
            this.button1.Text = "Select";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(351, 224);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 18;
            this.button2.Text = "Select";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // FrmPrivada
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(474, 481);
            this.Controls.Add(this.gbInPart);
            this.Controls.Add(this.gbNewPart);
            this.Name = "FrmPrivada";
            this.Text = "Invitar Amigos";
            this.Load += new System.EventHandler(this.FrmPrivada_Load);
            this.gbNewPart.ResumeLayout(false);
            this.gbNewPart.PerformLayout();
            this.gbInPart.ResumeLayout(false);
            this.gbInPart.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox gbNewPart;
        private System.Windows.Forms.TextBox txtNomSala;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox gbInPart;
        private System.Windows.Forms.TextBox txtInPar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnInPar;
        private System.Windows.Forms.TextBox txtPass;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblErrorPN;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPara;
        private System.Windows.Forms.Label lblErrorIP;
        private System.Windows.Forms.Button btnNuevaPar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbCantJug;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
    }
}