﻿namespace BlackJack
{
    partial class FrmMesa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMesa));
            this.lblEstado = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnApostar = new System.Windows.Forms.Button();
            this.btnReiApuesta = new System.Windows.Forms.Button();
            this.lblTotal = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btn50 = new System.Windows.Forms.Button();
            this.btn100 = new System.Windows.Forms.Button();
            this.btn500 = new System.Windows.Forms.Button();
            this.lblMiDinero = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblError = new System.Windows.Forms.Label();
            this.lblJug3 = new System.Windows.Forms.Label();
            this.lblJug1 = new System.Windows.Forms.Label();
            this.lblJug5 = new System.Windows.Forms.Label();
            this.lblJug7 = new System.Windows.Forms.Label();
            this.lblJug4 = new System.Windows.Forms.Label();
            this.lblJug6 = new System.Windows.Forms.Label();
            this.picApuesta = new System.Windows.Forms.PictureBox();
            this.lblJug2 = new System.Windows.Forms.Label();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.picb_play_3 = new System.Windows.Forms.PictureBox();
            this.picb_play_2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.picb_play_4 = new System.Windows.Forms.PictureBox();
            this.picb_play_1 = new System.Windows.Forms.PictureBox();
            this.picb_play_5 = new System.Windows.Forms.PictureBox();
            this.DilerGame = new System.Windows.Forms.Timer(this.components);
            this.btnPlantarse = new System.Windows.Forms.Button();
            this.btnNextCard = new System.Windows.Forms.Button();
            this.btnMic = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picApuesta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_play_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_play_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_play_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_play_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_play_5)).BeginInit();
            this.SuspendLayout();
            // 
            // lblEstado
            // 
            this.lblEstado.AutoSize = true;
            this.lblEstado.BackColor = System.Drawing.Color.Transparent;
            this.lblEstado.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstado.ForeColor = System.Drawing.Color.White;
            this.lblEstado.Location = new System.Drawing.Point(417, 226);
            this.lblEstado.Name = "lblEstado";
            this.lblEstado.Size = new System.Drawing.Size(22, 21);
            this.lblEstado.TabIndex = 14;
            this.lblEstado.Text = "...";
            // 
            // timer1
            // 
            this.timer1.Interval = 200;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnApostar);
            this.groupBox1.Controls.Add(this.btnReiApuesta);
            this.groupBox1.Controls.Add(this.lblTotal);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.btn50);
            this.groupBox1.Controls.Add(this.btn100);
            this.groupBox1.Controls.Add(this.btn500);
            this.groupBox1.Controls.Add(this.lblMiDinero);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 407);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(159, 246);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "My Money";
            // 
            // btnApostar
            // 
            this.btnApostar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnApostar.BackgroundImage")));
            this.btnApostar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnApostar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnApostar.ForeColor = System.Drawing.Color.White;
            this.btnApostar.Location = new System.Drawing.Point(75, 206);
            this.btnApostar.Name = "btnApostar";
            this.btnApostar.Size = new System.Drawing.Size(36, 31);
            this.btnApostar.TabIndex = 8;
            this.btnApostar.UseVisualStyleBackColor = true;
            this.btnApostar.Click += new System.EventHandler(this.btnApostar_Click);
            // 
            // btnReiApuesta
            // 
            this.btnReiApuesta.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnReiApuesta.BackgroundImage")));
            this.btnReiApuesta.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnReiApuesta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReiApuesta.ForeColor = System.Drawing.Color.White;
            this.btnReiApuesta.Location = new System.Drawing.Point(117, 206);
            this.btnReiApuesta.Name = "btnReiApuesta";
            this.btnReiApuesta.Size = new System.Drawing.Size(36, 31);
            this.btnReiApuesta.TabIndex = 7;
            this.btnReiApuesta.UseVisualStyleBackColor = true;
            this.btnReiApuesta.Click += new System.EventHandler(this.btnReiApuesta_Click);
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Location = new System.Drawing.Point(77, 77);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(20, 17);
            this.lblTotal.TabIndex = 6;
            this.lblTotal.Text = "...";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(77, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Total";
            // 
            // btn50
            // 
            this.btn50.BackgroundImage = global::BlackJack.Properties.Resources.moneda_50;
            this.btn50.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn50.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn50.ForeColor = System.Drawing.Color.White;
            this.btn50.Location = new System.Drawing.Point(6, 182);
            this.btn50.Name = "btn50";
            this.btn50.Size = new System.Drawing.Size(55, 55);
            this.btn50.TabIndex = 4;
            this.btn50.UseVisualStyleBackColor = true;
            this.btn50.Click += new System.EventHandler(this.btn50_Click);
            // 
            // btn100
            // 
            this.btn100.BackgroundImage = global::BlackJack.Properties.Resources.modena_100;
            this.btn100.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn100.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn100.ForeColor = System.Drawing.Color.White;
            this.btn100.Location = new System.Drawing.Point(6, 121);
            this.btn100.Name = "btn100";
            this.btn100.Size = new System.Drawing.Size(55, 55);
            this.btn100.TabIndex = 3;
            this.btn100.UseVisualStyleBackColor = true;
            this.btn100.Click += new System.EventHandler(this.btn100_Click);
            // 
            // btn500
            // 
            this.btn500.BackgroundImage = global::BlackJack.Properties.Resources.moenda_500;
            this.btn500.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn500.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn500.ForeColor = System.Drawing.Color.White;
            this.btn500.Location = new System.Drawing.Point(6, 60);
            this.btn500.Name = "btn500";
            this.btn500.Size = new System.Drawing.Size(55, 55);
            this.btn500.TabIndex = 2;
            this.btn500.UseVisualStyleBackColor = true;
            this.btn500.Click += new System.EventHandler(this.btn500_Click);
            // 
            // lblMiDinero
            // 
            this.lblMiDinero.AutoSize = true;
            this.lblMiDinero.Location = new System.Drawing.Point(56, 33);
            this.lblMiDinero.Name = "lblMiDinero";
            this.lblMiDinero.Size = new System.Drawing.Size(20, 17);
            this.lblMiDinero.TabIndex = 1;
            this.lblMiDinero.Text = "...";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Bank :";
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblError.Location = new System.Drawing.Point(183, 631);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(67, 17);
            this.lblError.TabIndex = 16;
            this.lblError.Text = "Mensaje...";
            // 
            // lblJug3
            // 
            this.lblJug3.AutoSize = true;
            this.lblJug3.BackColor = System.Drawing.Color.Transparent;
            this.lblJug3.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJug3.ForeColor = System.Drawing.Color.White;
            this.lblJug3.Location = new System.Drawing.Point(393, 467);
            this.lblJug3.Name = "lblJug3";
            this.lblJug3.Size = new System.Drawing.Size(46, 17);
            this.lblJug3.TabIndex = 17;
            this.lblJug3.Text = "Player";
            this.lblJug3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblJug1
            // 
            this.lblJug1.AutoSize = true;
            this.lblJug1.BackColor = System.Drawing.Color.Transparent;
            this.lblJug1.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJug1.ForeColor = System.Drawing.Color.White;
            this.lblJug1.Location = new System.Drawing.Point(564, 465);
            this.lblJug1.Name = "lblJug1";
            this.lblJug1.Size = new System.Drawing.Size(46, 17);
            this.lblJug1.TabIndex = 18;
            this.lblJug1.Text = "Player";
            this.lblJug1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblJug5
            // 
            this.lblJug5.AutoSize = true;
            this.lblJug5.BackColor = System.Drawing.Color.Transparent;
            this.lblJug5.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJug5.ForeColor = System.Drawing.Color.White;
            this.lblJug5.Location = new System.Drawing.Point(670, 423);
            this.lblJug5.Name = "lblJug5";
            this.lblJug5.Size = new System.Drawing.Size(46, 17);
            this.lblJug5.TabIndex = 19;
            this.lblJug5.Text = "Player";
            this.lblJug5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblJug7
            // 
            this.lblJug7.AutoSize = true;
            this.lblJug7.BackColor = System.Drawing.Color.Transparent;
            this.lblJug7.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJug7.ForeColor = System.Drawing.Color.White;
            this.lblJug7.Location = new System.Drawing.Point(285, 423);
            this.lblJug7.Name = "lblJug7";
            this.lblJug7.Size = new System.Drawing.Size(46, 17);
            this.lblJug7.TabIndex = 20;
            this.lblJug7.Text = "Player";
            this.lblJug7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblJug4
            // 
            this.lblJug4.AutoSize = true;
            this.lblJug4.BackColor = System.Drawing.Color.Transparent;
            this.lblJug4.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJug4.ForeColor = System.Drawing.Color.White;
            this.lblJug4.Location = new System.Drawing.Point(183, 356);
            this.lblJug4.Name = "lblJug4";
            this.lblJug4.Size = new System.Drawing.Size(46, 17);
            this.lblJug4.TabIndex = 21;
            this.lblJug4.Text = "Player";
            this.lblJug4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblJug6
            // 
            this.lblJug6.AutoSize = true;
            this.lblJug6.BackColor = System.Drawing.Color.Transparent;
            this.lblJug6.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJug6.ForeColor = System.Drawing.Color.White;
            this.lblJug6.Location = new System.Drawing.Point(756, 352);
            this.lblJug6.Name = "lblJug6";
            this.lblJug6.Size = new System.Drawing.Size(46, 17);
            this.lblJug6.TabIndex = 22;
            this.lblJug6.Text = "Player";
            this.lblJug6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // picApuesta
            // 
            this.picApuesta.BackColor = System.Drawing.Color.Transparent;
            this.picApuesta.Location = new System.Drawing.Point(471, 299);
            this.picApuesta.Name = "picApuesta";
            this.picApuesta.Size = new System.Drawing.Size(69, 60);
            this.picApuesta.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picApuesta.TabIndex = 23;
            this.picApuesta.TabStop = false;
            // 
            // lblJug2
            // 
            this.lblJug2.AutoSize = true;
            this.lblJug2.BackColor = System.Drawing.Color.Transparent;
            this.lblJug2.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblJug2.ForeColor = System.Drawing.Color.White;
            this.lblJug2.Location = new System.Drawing.Point(635, 135);
            this.lblJug2.Name = "lblJug2";
            this.lblJug2.Size = new System.Drawing.Size(46, 17);
            this.lblJug2.TabIndex = 24;
            this.lblJug2.Text = "Player";
            this.lblJug2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timer2
            // 
            this.timer2.Interval = 150;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // picb_play_3
            // 
            this.picb_play_3.Location = new System.Drawing.Point(584, 484);
            this.picb_play_3.Name = "picb_play_3";
            this.picb_play_3.Size = new System.Drawing.Size(48, 65);
            this.picb_play_3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picb_play_3.TabIndex = 25;
            this.picb_play_3.TabStop = false;
            // 
            // picb_play_2
            // 
            this.picb_play_2.Location = new System.Drawing.Point(530, 484);
            this.picb_play_2.Name = "picb_play_2";
            this.picb_play_2.Size = new System.Drawing.Size(48, 65);
            this.picb_play_2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picb_play_2.TabIndex = 26;
            this.picb_play_2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::BlackJack.Properties.Resources.images;
            this.pictureBox3.Location = new System.Drawing.Point(396, 397);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(48, 65);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 27;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(450, 397);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(48, 65);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 28;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(342, 356);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(48, 65);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 29;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::BlackJack.Properties.Resources.images;
            this.pictureBox6.Location = new System.Drawing.Point(288, 355);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(48, 65);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 30;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = global::BlackJack.Properties.Resources.images;
            this.pictureBox7.Location = new System.Drawing.Point(194, 284);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(48, 65);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 31;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = global::BlackJack.Properties.Resources.images;
            this.pictureBox8.Location = new System.Drawing.Point(248, 284);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(48, 65);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 32;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = global::BlackJack.Properties.Resources.images;
            this.pictureBox9.Location = new System.Drawing.Point(702, 355);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(48, 65);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox9.TabIndex = 34;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = global::BlackJack.Properties.Resources.images;
            this.pictureBox10.Location = new System.Drawing.Point(648, 356);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(48, 65);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox10.TabIndex = 33;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = global::BlackJack.Properties.Resources.images;
            this.pictureBox11.Location = new System.Drawing.Point(775, 284);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(48, 65);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox11.TabIndex = 36;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = global::BlackJack.Properties.Resources.images;
            this.pictureBox12.Location = new System.Drawing.Point(721, 284);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(48, 65);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox12.TabIndex = 35;
            this.pictureBox12.TabStop = false;
            // 
            // picb_play_4
            // 
            this.picb_play_4.Location = new System.Drawing.Point(638, 484);
            this.picb_play_4.Name = "picb_play_4";
            this.picb_play_4.Size = new System.Drawing.Size(48, 65);
            this.picb_play_4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picb_play_4.TabIndex = 39;
            this.picb_play_4.TabStop = false;
            // 
            // picb_play_1
            // 
            this.picb_play_1.Location = new System.Drawing.Point(476, 484);
            this.picb_play_1.Name = "picb_play_1";
            this.picb_play_1.Size = new System.Drawing.Size(48, 65);
            this.picb_play_1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picb_play_1.TabIndex = 40;
            this.picb_play_1.TabStop = false;
            // 
            // picb_play_5
            // 
            this.picb_play_5.Location = new System.Drawing.Point(692, 484);
            this.picb_play_5.Name = "picb_play_5";
            this.picb_play_5.Size = new System.Drawing.Size(48, 65);
            this.picb_play_5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picb_play_5.TabIndex = 41;
            this.picb_play_5.TabStop = false;
            // 
            // DilerGame
            // 
            this.DilerGame.Interval = 2000;
            this.DilerGame.Tick += new System.EventHandler(this.DilerGame_Tick);
            // 
            // btnPlantarse
            // 
            this.btnPlantarse.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPlantarse.BackgroundImage")));
            this.btnPlantarse.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPlantarse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPlantarse.ForeColor = System.Drawing.Color.White;
            this.btnPlantarse.Location = new System.Drawing.Point(746, 545);
            this.btnPlantarse.Name = "btnPlantarse";
            this.btnPlantarse.Size = new System.Drawing.Size(90, 99);
            this.btnPlantarse.TabIndex = 9;
            this.btnPlantarse.UseVisualStyleBackColor = true;
            this.btnPlantarse.Click += new System.EventHandler(this.btnPlantarse_Click);
            // 
            // btnNextCard
            // 
            this.btnNextCard.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNextCard.BackgroundImage")));
            this.btnNextCard.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnNextCard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNextCard.ForeColor = System.Drawing.Color.White;
            this.btnNextCard.Location = new System.Drawing.Point(853, 545);
            this.btnNextCard.Name = "btnNextCard";
            this.btnNextCard.Size = new System.Drawing.Size(90, 99);
            this.btnNextCard.TabIndex = 42;
            this.btnNextCard.UseVisualStyleBackColor = true;
            this.btnNextCard.Click += new System.EventHandler(this.btnCarta_Click);
            // 
            // btnMic
            // 
            this.btnMic.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnMic.BackgroundImage")));
            this.btnMic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMic.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMic.ForeColor = System.Drawing.Color.White;
            this.btnMic.Location = new System.Drawing.Point(876, 437);
            this.btnMic.Name = "btnMic";
            this.btnMic.Size = new System.Drawing.Size(67, 64);
            this.btnMic.TabIndex = 43;
            this.btnMic.UseVisualStyleBackColor = true;
            this.btnMic.Click += new System.EventHandler(this.btnMic_Click);
            // 
            // FrmMesa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(964, 665);
            this.Controls.Add(this.btnMic);
            this.Controls.Add(this.btnNextCard);
            this.Controls.Add(this.btnPlantarse);
            this.Controls.Add(this.picb_play_5);
            this.Controls.Add(this.picb_play_1);
            this.Controls.Add(this.picb_play_4);
            this.Controls.Add(this.pictureBox11);
            this.Controls.Add(this.pictureBox12);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.pictureBox10);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.picb_play_2);
            this.Controls.Add(this.picb_play_3);
            this.Controls.Add(this.lblJug2);
            this.Controls.Add(this.picApuesta);
            this.Controls.Add(this.lblJug6);
            this.Controls.Add(this.lblJug4);
            this.Controls.Add(this.lblJug7);
            this.Controls.Add(this.lblJug5);
            this.Controls.Add(this.lblJug1);
            this.Controls.Add(this.lblJug3);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblEstado);
            this.Name = "FrmMesa";
            this.Text = "c x                                                                           ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMesa_FormClosing);
            this.Load += new System.EventHandler(this.FrmMesa_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picApuesta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_play_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_play_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_play_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_play_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picb_play_5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblEstado;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn50;
        private System.Windows.Forms.Button btn100;
        private System.Windows.Forms.Button btn500;
        private System.Windows.Forms.Label lblMiDinero;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnApostar;
        private System.Windows.Forms.Button btnReiApuesta;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.Label lblJug3;
        private System.Windows.Forms.Label lblJug1;
        private System.Windows.Forms.Label lblJug5;
        private System.Windows.Forms.Label lblJug7;
        private System.Windows.Forms.Label lblJug4;
        private System.Windows.Forms.Label lblJug6;
        private System.Windows.Forms.PictureBox picApuesta;
        private System.Windows.Forms.Label lblJug2;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.PictureBox picb_play_3;
        private System.Windows.Forms.PictureBox picb_play_2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox picb_play_4;
        private System.Windows.Forms.PictureBox picb_play_1;
        private System.Windows.Forms.PictureBox picb_play_5;
        private System.Windows.Forms.Timer DilerGame;
        private System.Windows.Forms.Button btnPlantarse;
        private System.Windows.Forms.Button btnNextCard;
        private System.Windows.Forms.Button btnMic;
    }
}