﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackJackENL;
using BLackJackDAL;

namespace BlackJackBOL
{
    public class ReporteBOL
    {
        private ReporteDAL rdal;

        public ReporteBOL()
        {
            rdal = new ReporteDAL();
        }
        /// <summary>
        /// Hace el Reporte del Usuario
        /// </summary>
        /// <param name="miReporte"></param>
        public void DoMyReport(EReporte miReporte)
        {
           rdal.DoMyReport(miReporte);
        }
        /// <summary>
        /// Conecta con El DAL para cargar los reportes
        /// </summary>
        /// <param name="homeUser">Usser</param>
        /// <param name="aux">Fecha</param>
        /// <returns>Lista de Usuarios</returns>
        public List<EReporte> TakeByDate(EUsser homeUser, DateTime aux)
        {
            return rdal.TakeByDate(homeUser,aux);
        }
    }
}
