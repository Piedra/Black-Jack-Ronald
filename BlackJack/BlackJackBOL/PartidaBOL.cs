﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLackJackDAL;
using BlackJackENL;

namespace BlackJackBOL
{
    public class PartidaBOL
    {
        private PartidaDAL jdal;

        public PartidaBOL()
        {
            jdal = new PartidaDAL();
        }
        /// <summary>
        /// Conecta Con el DAL para buscar la partida publica
        /// </summary>
        /// <param name="nativeUsser">usuario </param>
        /// <returns>EPartida</returns>
        public EPartida BuscarPartidaPub(EUsser nativeUsser)
        {
            return jdal.BuscarPartida(nativeUsser);
        }
        /// <summary>
        /// Conecta Con el DAL para Cargar una Partida  por id
        /// </summary>
        /// <param name="partida">id de la partida</param>
        /// <returns>Partida Cargada</returns>
        public EPartida CargarPartida(EPartida partida)
        {
            return jdal.CargarPartidaID(partida.Id);
        }
        /// <summary>
        /// Conecta Con el DAL para realizar la accion de plantarse
        /// </summary>
        /// <param name="partida">partida en la que se planta</param>
        public void Plantarse(EPartida partida)
        {
            jdal.Actualizar(partida);
        }
        /// <summary>
        /// Conecta Con el DAL para restarle Dinero Al Usuario
        /// </summary>
        /// <param name="usu">Usuario a quien resta</param>
        /// <param name="cantidad">Cuanto restar</param>
        /// <returns>Bool True: Si False: No</returns>
        public bool RestarDinero(EUsser usu, int cantidad)
        {
            return jdal.RestarDinero(usu, cantidad);
        }
        /// <summary>
        /// Conecta Con el DAL para Consigue un Nuevo deck
        /// </summary>
        /// <param name="par">para la partida</param>
        /// <returns>id del deck</returns>
        public string GetMeDeck(EPartida par)
        {
            return jdal.GetMeDeck(par);
        }
        /// <summary>
        /// Conecta Con el DAL para Saca de la Partida a un Usuario
        /// </summary>
        /// <param name="usu">Usuario a SACAR</param>
        /// <param name="par">Partida de la cual sacarlo</param>
        /// <returns>Bool True: Si False: No</returns>
        public bool TakeOutOfRoom(EUsser usu, EPartida par)
        {
             return jdal.TakeOutOfRoom(usu,par.Id);
        }
        /// <summary>
        /// Conecta Con el DAL para Ponerle un Diler a La Tabla
        /// </summary>
        /// <param name="part">Bool True: Si False: No</param>
        public void SetDilerToTable(EPartida part)
        {
            jdal.SetDilerToTable(part);
        }
        /// <summary>
        /// Conecta Con el DAL para
        /// </summary>
        /// <param name="aux"></param>
        /// <param name="partida"></param>
        /// <returns></returns>
        public bool SetNewDeck(EDeck aux,EPartida partida)
        {
            return jdal.ActualizarDeck(aux,partida.Id);
        }
        /// <summary>
        /// Conecta Con el DAL para Actualizar el deck de la partida
        /// </summary>
        /// <param name="par">Partida a cambiar deck</param>
        /// <returns>Epartida con el nuevo deck </returns>
        public EPartida NuevaPartida(EPartida par)
        {
            
            return jdal.NuevaPartida(par);
        }
        /// <summary>
        /// Conecta Con el DAL para Hacer una nueva partida
        /// </summary>
        /// <param name="usu">Usuario </param>
        /// <param name="pass">COntraseña de la partida</param>
        /// <returns></returns>
        public EPartida BuscarPartidaPri(EUsser usu, string pass)
        {
            return jdal.BuscarPartida(usu,pass);
        }
        // <summary>
        /// Conecta Con el DAL para Hacer una nueva partida por primera vez
        /// </summary>
        /// <param name="usu">Usuario </param>
        /// <param name="pass">COntraseña de la partida</param>
        /// <returns></returns>
        public EPartida BuscarPartidaPrivadaF(EUsser usu, string pass)
        {
            return jdal.BuscarPartidaFirst(usu, pass);
        }
        /// <summary>
        /// Conecta Con el DAL para cargar un partida
        /// </summary>
        /// <param name="nom">nombre de la partida</param>
        /// <param name="pass">contraseña de la partida</param>
        /// <returns></returns>
        public EPartida CargarPrivada(string nom, string pass)
        {
            return jdal.CargarPrivada(nom, pass);
        }

    }
}
