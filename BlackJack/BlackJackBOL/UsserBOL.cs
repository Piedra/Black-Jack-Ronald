﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackJackENL;
using BLackJackDAL;


namespace BlackJackBOL
{
    public class UsserBOL
    {
        private UsserDAL udal;

        public UsserBOL()
        {
            udal = new UsserDAL();
        }
        /// <summary>
        /// Conecta con El DAL para Validar 
        /// </summary>
        /// <param name="uss">Usuarop a validar</param>
        /// <returns>usuario Validado</returns>
        public EUsser Validate(EUsser uss)
        {
            return udal.Validate(uss);
        }
        /// <summary>
        /// Conecta con el DAL para recargar dinero al Usuario
        /// </summary>
        /// <param name="user">Usuario</param>
        /// <param name="cantidad">Canitdad a inmgresar</param>
        /// <returns>Usuariio con los datos cargados</returns>
        public EUsser Recargar(EUsser user,int cantidad)
        {
            return udal.Recargar(user,cantidad);
        }
        /// <summary>
        /// Carga por id a un Usairo
        /// </summary>
        /// <param name="id">Usurio a Buscar</param>
        /// <returns>Usuario cargado</returns>
        public EUsser CargarPorId(int id)
        {
            return udal.CargarPorId(id);
        }
        /// <summary>
        /// Conecta con el DAL para Cargar los usuarios en una partida
        /// </summary>
        /// <param name="partida">Partida en que buscar</param>
        /// <returns>Lista de Usuarios</returns>
        public List<EUsser> CargarUsuariosEnPartida(EPartida partida)
        {
            return udal.CargarPorPartida(partida.Id);
        }
        /// <summary>
        /// Conecta con el DAL Para Refrescar
        /// </summary>
        /// <param name="id">Id a quien refrescar </param>
        /// <returns>user refrescado</returns>
        public EUsser Refresh(int id)
        {
            return udal.Refresh(id);
        }
    }


}
