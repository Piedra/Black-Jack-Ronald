﻿using BLackJackDAL;
using BlackJackENL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJackBOL
{
    public class FacebookBOL
    {
        private FacebookDAL fdal;

        public FacebookBOL()
        {
            fdal = new FacebookDAL();
        }
        /// <summary>
        /// Conecta con el DAL para conseguir el link de Login.
        /// </summary>
        /// <param name="fec">EFacebook</param>
        /// <returns>link</returns>
        public Uri GetLoginUrl(EFacebook fec)
        {
            return fdal.GetLoginUrl(fec);
        }
    }
}
