﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJackENL
{
    public class ECarta
    {
        public string deck_id { get; set; }
        public Card[] cards { get; set; }
        public bool success { get; set; }
        public int remaining { get; set; }
    }

    public class Card
    {
        public string suit { get; set; }
        public Images images { get; set; }
        public string value { get; set; }
        public string image { get; set; }
        public string code { get; set; }
    }

    public class Images
    {
        public string png { get; set; }
        public string svg { get; set; }
    }
}
