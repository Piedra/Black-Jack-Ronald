﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJackENL
{
    public class EDeck
    {
        public string deck_id { get; set; }
        public bool success { get; set; }
        public int remaining { get; set; }
        public bool shuffled { get; set; }
    }
}
