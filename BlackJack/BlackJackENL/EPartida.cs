﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJackENL
{
    public class EPartida
    {
        public int Id { get; set; }
        public int CantJugadores { get; set; }
        public string Name { get; set; }
        public int JugadorActual { get; set; }
        public string Id_Deck { get; set; }
        public string Password { get; set; }
    }
}
