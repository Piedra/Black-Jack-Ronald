﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJackENL
{
    public class EReporte
    {
        public int Id { get; set; }
        public int Id_jug { get; set; }
        public int Inicial { get; set; }
        public int Final { get; set; }
        public DateTime Fecha { get; set; }
    }
}
