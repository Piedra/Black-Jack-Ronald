﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackJackENL;
using Npgsql;

namespace BLackJackDAL
{
    public class UsserDAL
    {
        /// <summary>
        /// Se Encarga de Validar Si Un Usuario Ya Exite Y si no Lo Crea Nuevo
        /// </summary>
        /// <param name="uss">Usuario a Validar</param>
        /// <returns>Un Usuario Cargado</returns>
        public EUsser Validate(EUsser uss)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                con.Open();
                string sql = @"SELECT id,idusser,name, email, money, picture, active
	                        FROM usser where (email = @email) ";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@email", uss.Email);
                NpgsqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    return CargarUsuario(reader);
                }
                else
                {
                    int usId = NuevoUsuario(uss);
                    return CargarPorId(usId);
                }
            }
        }
        /// <summary>
        /// Se Encarga de Refrescar Los datos del Usuario
        /// </summary>
        /// <param name="id">Id del Usuario para ser Buscado</param>
        /// <returns>EL Usuario Refrescado</returns>
        public EUsser Refresh(int id)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                con.Open();
                string sql = @"SELECT id, idusser, name, email, money, picture, active
	                        FROM usser WHERE id = @id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", id);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    return CargarUsuario(reader);
                }
            }
            return null;
        }
        /// <summary>
        /// Carga una lista de Usuarios que se Encuentren en la Partida
        /// </summary>
        /// <param name="id">Id del usuario a buscar</param>
        /// <returns>Lista De Usuarios</returns>
        public List<EUsser> CargarPorPartida(int id)
        {
            List<EUsser> ussers = new List<EUsser>();
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                con.Open();
                string sql = @"SELECT id_jug
	                FROM par_usu WHERE id_par= @par;";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@par", id);
                NpgsqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ussers.Add(CargarPorId(CargarPartUsu(reader)));
                }

            }

            return ussers;

        }
        /// <summary>
        /// Toma un Reader Proveniente de la BD y lo Hace Par_usu
        /// </summary>
        /// <param name="reader">Reader se encargar de leer datos de LA BD</param>
        /// <returns>Id Del Par_usu</returns>
        private int  CargarPartUsu(NpgsqlDataReader reader)
        {
            return Convert.ToInt32(reader["id_jug"]);
        }
        /// <summary>
        /// Recarga una Canitdad de Dinero a un Usuario
        /// </summary>
        /// <param name="user">Usuario a recargar</param>
        /// <param name="cantidad">Cantidad a Recargar </param>
        /// <returns>Usuario con los datos Actualizados</returns>
        public EUsser Recargar(EUsser user,int cantidad)
        {
            int variable = 0;
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                con.Open();
                string sql = @"UPDATE public.usser
	                SET  money= money + @cant
	                WHERE id = @id returning id;";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", user.Id);
                cmd.Parameters.AddWithValue("@cant",cantidad);
                variable = Convert.ToInt32(cmd.ExecuteScalar());
                return CargarPorId(variable); 
            }
        }
        /// <summary>
        /// Carga un usuario a partir del Reader
        /// </summary>
        /// <param name="reader">Reader se encargar de leer datos de LA BD</param>
        /// <returns>Usuario Cargado</returns>
        private EUsser CargarUsuario(NpgsqlDataReader reader)
        {
            byte[] f = new byte[0];
            f = (byte[])reader["picture"];
            MemoryStream stream = new MemoryStream(f);

            EUsser auxUsser = new EUsser
            {
                Id = reader["id"] != DBNull.Value ? Convert.ToInt32(reader["id"]) : 0,
                UsserId = reader["idusser"].ToString(),
                Name = reader["name"].ToString(),
                Email = reader["email"].ToString(),
                Money = Convert.ToDouble(reader["money"]),
                PhotoOfUsser = Image.FromStream(stream),
                Active = (bool)reader["active"],

            };
            return auxUsser;
        }
        /// <summary>
        /// Hace un Nuevo Usuario Devolviendo el Int
        /// </summary>
        /// <param name="usu">El Usuario a Hacer Nuevo</param>
        /// <returns>El int del Usuario</returns>
        private int NuevoUsuario(EUsser usu)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                con.Open();
                string sql = @"INSERT INTO public.usser(
	            idusser, name, email, money,picture, active)
	            VALUES (@idusu, @name, @emial, @mon, @pic, @activ) returning id;";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@idusu", usu.UsserId);
                cmd.Parameters.AddWithValue("@name", usu.Name);
                cmd.Parameters.AddWithValue("@emial", usu.Email);
                cmd.Parameters.AddWithValue("@mon", usu.Money);
                MemoryStream stream = new MemoryStream();
                usu.PhotoOfUsser.Save(stream, ImageFormat.Jpeg);
                byte[] pic = stream.ToArray();
                cmd.Parameters.AddWithValue("@pic", pic);
                cmd.Parameters.AddWithValue("@activ", true);
                return Convert.ToInt32(cmd.ExecuteScalar());

            }

        }
        /// <summary>
        /// Carga por Id a el Usuario
        /// </summary>
        /// <param name="usId">Id del usuario a Cargar</param>
        /// <returns>Usuario Cargado </returns>
        public EUsser CargarPorId(int usId)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                con.Open();
                string sql = @"SELECT id,idusser,name, email, money, picture, active
	                        FROM usser where (id = @id)";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", usId);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    return CargarUsuario(reader);
                }
            }
            return null;
        }


    }

}
