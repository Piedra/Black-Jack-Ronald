﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackJackENL;
using Npgsql;

namespace BLackJackDAL
{
    public class PartidaDAL
    {
        /// <summary>
        /// Se encarga de ingresar a la Tabla par_usu al usuario y a la partida que pertence
        /// </summary>
        /// <param name="nativeUsser">usuario a guardar</param>
        /// <returns>EPartida Cargdda</returns>
        public EPartida BuscarPartida(EUsser nativeUsser)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"insert into par_usu(id_jug, id_par) 
                               values (@id,
                                      (select id from partida where pass like '' limit 1)) 
                               returning id_par";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", nativeUsser.Id);
                int idPar = Convert.ToInt32(cmd.ExecuteScalar());
                return CargarPartidaID(idPar);
            }
        }
        /// <summary>
        /// Se encarga de ingresar a la Tabla par_usu al usuario y a la partida que pertence
        /// y tenga contraseña que coincida
        /// </summary>
        /// <param name="nativeUsser">usuario a guardar</param>
        /// <returns>EPartida Cargdda</returns>
        public EPartida BuscarPartida(EUsser nativeUsser, string pass)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"insert into par_usu(id_jug, id_par) 
                               values (@id,
                                      (select id from partida where pass like @pas limit 1)) 
                               returning id_par";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", nativeUsser.Id);
                cmd.Parameters.AddWithValue("@pas", pass);
                int idPar = Convert.ToInt32(cmd.ExecuteScalar());
                return CargarPartidaID(idPar);
            }
        }
        /// <summary>
        /// Se encarga de ingresar a la Tabla par_usu al usuario y a la partida que pertence
        /// y tenga contraseña que coincida y sea la primera vez que se crea esa partida
        /// </summary>
        /// <param name="nativeUsser">usuario a guardar</param>
        /// <returns>EPartida Cargdda</returns>
        public EPartida BuscarPartidaFirst(EUsser nativeUsser, string pass)
        {
            CallDilerForRoom(pass);
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"insert into par_usu(id_jug, id_par) 
                               values (@id,
                                      (select id from partida where pass like @pas limit 1)) 
                               returning id_par";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", nativeUsser.Id);
                cmd.Parameters.AddWithValue("@pas", pass);
                int idPar = Convert.ToInt32(cmd.ExecuteScalar());
                return CargarPartidaID(idPar);
            }
        }
        /// <summary>
        /// Carga una Partida Privada Por nombre y Contraseña
        /// </summary>
        /// <param name="nom">Nombre de la Partida</param>
        /// <param name="pass">Contraseña de la Partida</param>
        /// <returns></returns>
        public EPartida CargarPrivada(string nom, string pass)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"SELECT id, pass, cant_jug, jug_actual, id_deck, nombre
	                    FROM partida WHERE pass=@pass and nombre = @nom;";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@nom", nom);
                cmd.Parameters.AddWithValue("@pass", pass);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    return CargarPartida(reader);
                }
            }
            return null;
        }
        /// <summary>
        /// Consigue un Nuevo deck y lo coloca en la partida indicada
        /// </summary>
        /// <param name="par">Partida que solicita Deck</param>
        /// <returns>Devolvemos el Ip de deck</returns>
        public string GetMeDeck(EPartida par)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"SELECT id_deck
	                        FROM public.partida Where id = @id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", par.Id);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    return reader["id_deck"].ToString();
                }
            }
            return "";
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="par"></param>
        /// <returns></returns>
        public EPartida NuevaPartida(EPartida par)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"INSERT INTO public.partida(
	                    pass, cant_jug, jug_actual, id_deck, nombre)
	                    VALUES (@pass, @cant, @act, @deck,@name) returning id;";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@pass", par.Password);
                cmd.Parameters.AddWithValue("@cant", par.CantJugadores);
                cmd.Parameters.AddWithValue("@act", par.JugadorActual);
                cmd.Parameters.AddWithValue("@deck", par.Id_Deck);
                cmd.Parameters.AddWithValue("@name", par.Name);
                Console.WriteLine(par.JugadorActual);
                object id = cmd.ExecuteScalar();
                return CargarPartidaID((int)id);
            };
        }
        /// <summary>
        /// Coloca un Diler en una Tabla
        /// </summary>
        /// <param name="part">Tabla a la cual se desea ingresar el diler </param>
        /// <returns>Bool: True: Ingreso False: No </returns>
        public bool SetDilerToTable(EPartida part)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"UPDATE public.partida
	                        SET jug_actual = @valor
	                         WHERE id = @id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@valor", 2);
                cmd.Parameters.AddWithValue("@id", part.Id);
                return cmd.ExecuteNonQuery() > 0;
            }
        }
        /// <summary>
        /// Me saca de una partida a la que deseo salir
        /// </summary>
        /// <param name="usu">El Usuario que desea salir</param>
        /// <param name="par">A la partida que desea salir</param>
        /// <returns></returns>
        public bool TakeOutOfRoom(EUsser usu, int par)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"DELETE FROM par_usu 
                                WHERE id_jug = @id and id_par = @par;";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", usu.Id);
                cmd.Parameters.AddWithValue("@par", par);
                return cmd.ExecuteNonQuery() > 0;
            }

        }
        /// <summary>
        /// Me pide un Nuevo deck para ser Actualizado
        /// </summary>
        /// <param name="deck">El Deck A Actualiazar</param>
        /// <param name="id">id de la Partida</param>
        /// <returns></returns>
        public bool ActualizarDeck(EDeck deck, int id)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"UPDATE public.partida
	                    SET id_deck=@deck
	                    WHERE id = @id;";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@deck", deck.deck_id);
                cmd.Parameters.AddWithValue("@id", id);
                return cmd.ExecuteNonQuery() > 0;
            }
        }
        /// <summary>
        /// Le resta una Cantidad de Dinero al usuario
        /// </summary>
        /// <param name="usu">Usuario A Restar</param>
        /// <param name="cantidad">Cantidad a Restar</param>
        /// <returns></returns>
        public bool RestarDinero(EUsser usu, int cantidad)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"UPDATE public.usser
	                        SET money = money - @money
	                        WHERE id = @id;";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@money", cantidad);
                cmd.Parameters.AddWithValue("@id", usu.Id);
                return cmd.ExecuteNonQuery() > 0;
            }
        }
        /// <summary>
        /// Buca una Partida por id y la carga
        /// </summary>
        /// <param name="idPar">id de la Partida</param>
        /// <returns></returns>
        public EPartida CargarPartidaID(int idPar)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"SELECT  id, pass, cant_jug, jug_actual, id_deck, nombre
	                        FROM public.partida; 
                            where id = @id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", idPar);
                NpgsqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    return CargarPartida(reader);
                }
            }
            return null;

        }
        /// <summary>
        /// Toma el reader y hace una partida de el
        /// </summary>
        /// <param name="reader">Reader se encargar de leer datos de LA BD<</param>
        /// <returns>Partida Cargada</returns>
        private EPartida CargarPartida(NpgsqlDataReader reader)
        {
            return new EPartida
            {
                Id = reader.GetInt32(0),
                Password = reader.GetString(1),
                CantJugadores = reader.GetInt32(2),
                JugadorActual = reader.GetInt32(3),
                Id_Deck = reader.GetString(4),
                Name = reader.GetString(5)
            };
        }
        /// <summary>
        /// Actualiza la Partida en la que estoy jugando
        /// </summary>
        /// <param name="partida">partida a la que deseo actualizar</param>
        public void Actualizar(EPartida partida)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"update partida set jug_actual = @sig where id = @id";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", partida.Id);
                cmd.Parameters.AddWithValue("@sig", Siguiente(partida));
                cmd.ExecuteNonQuery();
            }
        }
        /// <summary>
        /// Indica Cual es el siguiente Jugador en la mesa
        /// </summary>
        /// <param name="partida">paritda a investigar</param>
        /// <returns>numero del siguiente</returns>
        private int Siguiente(EPartida partida)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"select id_jug from par_usu
                                where id_par = @idP and orden > (select orden from par_usu where id_jug = @idJ and id_par = @idP)
                                order by orden limit 1";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@idP", partida.Id);
                cmd.Parameters.AddWithValue("@idJ", partida.JugadorActual);
                object o = cmd.ExecuteScalar();
                int id = o == DBNull.Value ? 0 : Convert.ToInt32(o);
                if (id > 0)
                {
                    return id;
                }
                cmd.Parameters.Clear();
                sql = @"select id_jug from par_usu
                                where id_par = @idP 
                                order by orden limit 1";
                cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@idP", partida.Id);
                o = cmd.ExecuteScalar();
                id = o == DBNull.Value ? 0 : Convert.ToInt32(o);
                return id;
            }
        }
        /// <summary>
        /// coloca un diler para la Mesa con COntraseña
        /// </summary>
        /// <param name="pass">Contraseña de la mesa</param>
        private void CallDilerForRoom(string pass)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"insert into par_usu(id_jug, id_par) 
                               values (@id,
                                      (select id from partida where pass like @pas limit 1))";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@id", 2);
                cmd.Parameters.AddWithValue("@pas", pass);
                cmd.ExecuteNonQuery();
            }
        }

    }
}
