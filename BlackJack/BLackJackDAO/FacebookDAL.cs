﻿using BlackJackENL;
using Facebook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLackJackDAL
{
    public class FacebookDAL
    {
        /// <summary>
        /// Consigue el Link del Login de Facebook
        /// </summary>
        /// <param name="face">Clase facebook</param>
        /// <returns></returns>
        public Uri GetLoginUrl(EFacebook face)
        {
            FacebookClient a = new FacebookClient();
            var fbLoginUri = a.GetLoginUrl(new
            {
                client_id = face.AppId,
                redirect_uri = "https://www.facebook.com/connect/login_success.html",
                response_type = "code",
                display = "popup",
                scope = "email"
            });
            return fbLoginUri;
        }
        /// <summary>
        /// Consigue el link de logput de facebook
        /// </summary>
        /// <param name="face">Clase facebook</param>
        /// <returns></returns>
        public Uri GetLogOutUrl(EFacebook face)
        {
            FacebookClient a = new FacebookClient();
            var fbLoginUri = a.GetLogoutUrl(new
            {
                client_id = face.AppId,
                redirect_uri = "https://www.facebook.com/connect/login_success.html",
                response_type = "code",
                display = "popup",
                scope = "email"
            });
            return fbLoginUri;
        }
        
    }
}
