﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackJackENL;
using Npgsql;

namespace BLackJackDAL
{
    public class ReporteDAL
    {
        /// <summary>
        /// Hace un Reporte y lo guarda en la Base de Datos
        /// </summary>
        /// <param name="miReporte">Reporte a Guardae</param>
        public void DoMyReport(EReporte miReporte)
        {
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"INSERT INTO public.reporte(
	                        id_jug, inicial, final, fecha)
	                        VALUES (@jug, @ini, @fini, @fecha);";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@jug", miReporte.Id_jug);
                cmd.Parameters.AddWithValue("@ini", miReporte.Inicial);
                cmd.Parameters.AddWithValue("@fini", miReporte.Final);
                cmd.Parameters.AddWithValue("@fecha", miReporte.Fecha);
                cmd.ExecuteNonQuery();
            };
        }
        /// <summary>
        /// Slecciona de La Base de datos los reportes en una fecha determinada
        /// </summary>
        /// <param name="homeUser">Usuario a Buscar</param>
        /// <param name="aux">Fecha a buscar </param>
        /// <returns>Lista con Reportes</returns>
        public List<EReporte> TakeByDate(EUsser homeUser, DateTime aux)
        {
            List<EReporte> reportes = new List<EReporte>();
            using (NpgsqlConnection con = new NpgsqlConnection(Configuracion.ConStr))
            {
                //Abrir una conexion
                con.Open();
                //Definir la consulta
                string sql = @"SELECT id, id_jug, inicial, final, fecha
	                        FROM reporte WHERE id_jug=@jug and fecha = CAST(@fecha AS DATE);";
                NpgsqlCommand cmd = new NpgsqlCommand(sql, con);
                Console.WriteLine(homeUser.Id);
                cmd.Parameters.AddWithValue("@jug", homeUser.Id);
                cmd.Parameters.AddWithValue("@fecha", aux.ToShortDateString());
                NpgsqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Console.WriteLine("One");
                    reportes.Add(CargarReporte(reader));
                }
            };
            return reportes;
        }
        /// <summary>
        /// Toma un Reader y forma un Reporte
        /// </summary>
        /// <param name="reader">Reader se encargar de leer datos de LA BD</param>
        /// <returns>Reporte Cargado</returns>
        private EReporte CargarReporte(NpgsqlDataReader reader)
        {
            EReporte repo = new EReporte
            {
                Id = Convert.ToInt32(reader["id"]),
                Id_jug = Convert.ToInt32(reader["id_jug"]),
                Inicial = Convert.ToInt32(reader["inicial"]),
                Final = Convert.ToInt32(reader["final"]),
                Fecha = Convert.ToDateTime(reader["fecha"].ToString())
            };
            return repo;
        }
    }
}
